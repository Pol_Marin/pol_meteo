import React, { useState, useEffect } from "react";

import { Row, Col } from "reactstrap";
import styled from "styled-components";

const IconoCentrado = styled.i`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;
  width: 100%;
`;


const Meteo = () => {

    const [llista, setLlista] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const ciutat = "barcelona,es";
        const apiKey = "1cd05e454bba35902d6b794b9096666f";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                setLlista(data.list);
                setLoading(false);
            })
            .catch((error) => setError(true));

        console.log("peticion enviada");

    }, []);

    if (error) {
        return <h3>Se ha producido un error...</h3>;
    }

    if (loading) {
        return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
    }

    let daysContent = llista.filter((el, idx) => idx % 8 === 0)
        .map((el, idx) => {

            el.date = new Date(el.dt * 1000);
            el.date = el.date.toLocaleString();

            let sep = () => el.date.split(" ");

            el.fecha = sep()[0];
            if (idx === 0) el.fecha = "HOY";

            let time = sep()[1].split(":");
            let clock = time[0] + ":" + time[1] + "h";

            el.icon = el.weather[0].icon;

            el.temp = el.main.temp;

            let baColor = " ";

            if (el.icon === "01d") baColor = "sunny";
            else if (el.icon === "02d") baColor = "sun-cloud";
            else if (el.icon === "03d") baColor = "cloud";
            else if (el.icon === "04d") baColor = "clouds";
            else if (el.icon === "09d") baColor = "rain";
            else if (el.icon === "10d") baColor = "sun-rain";
            else if (el.icon === "11d") baColor = "storm";
            else if (el.icon === "13d") baColor = "snow";
            else if (el.icon === "50d") baColor = "fog";
            else baColor = "night"

            return (
                <Col>
                    <div className="day mt-4 ">
                        <h4>{el.fecha}</h4>
                        <div className={`info ${baColor}`}>
                            <h5><strong>{clock}</strong></h5>
                            <img src={`http://openweathermap.org/img/wn/${el.icon}@2x.png`} alt={el.icon} />
                            <h5><strong>Temp: </strong>{el.temp}º</h5>
                        </div>
                    </div>
                </Col>
            )
        })

    let today = daysContent[0];

    let nextDays = daysContent.filter((el, idx) => idx !== 0);

    return (
        <div className="contenedor">
            <Row>
                    {today}
            </Row>
            <Row>
                {nextDays}
            </Row>
        </div>
    );
};

export default Meteo;
